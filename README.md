
# Projet de Veille Informatique avec API Hacker News

Ce projet est une application de veille informatique qui utilise l'API Hacker News pour afficher les dernières nouvelles technologiques. Il est développé en JavaScript en suivant une architecture Modèle-Vue-Contrôleur (MVC) et comprend des tests unitaires pour assurer la qualité du code.


# Ressources API

https://hackernews.api-docs.io/v0/overview/introduction
https://github.com/HackerNews/API
https://www.postman.com/devrel/workspace/hackernews/documentation/13191452-f8f00d06-994f-4d1a-b0b5-2df4800fa64d